[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/sscsalex/frontend) [![Netlify Status](https://api.netlify.com/api/v1/badges/3d60a126-d1e6-4296-8d62-620777b2dd10/deploy-status)](https://app.netlify.com/sites/sscsalex/deploys)

# IEEE SSCS AlexSC Website Frontend

Builder of the IEEE SSCS AlexSC Website Frontend https://sscsalex.org

Derived from [gatsby-starter-tailwind](https://github.com/taylorbryant/gatsby-starter-tailwind).

## Running the Project.

To run this project for the first time run `npm install`, then:

1. Run the CMS from https://gitlab.com/sscsalex/cms
2. Run `npm run dev`

Once running, the frontend will be reachable via http://localhost:8000
