const tailwindcss = require('tailwindcss');

module.exports = {
	plugins: [
		require('postcss-import'),
		tailwindcss('./tailwind.config.js'),
		require('postcss-preset-env'),
		...(process.env.NODE_ENV === `production` ? [require(`cssnano`)] : [])
	]
};
