// See https://tailwindcss.com/docs/configuration for details
const { fontFamily } = require('tailwindcss/defaultTheme');

module.exports = {
	purge: ['./src/**/*.jsx'],
	theme: {
		fontFamily: {
			sans: ['"Open Sans"', ...fontFamily.sans]
		},
		extend: {
			colors: {
				'bright-red': {
					100: '#BA0C2F',
					80: '#D55154',
					60: '#E47E7B',
					40: '#EFA8A4',
					20: '#F8D2D0'
				},
				'dark-red': {
					100: '#861F41',
					80: '#A54E62',
					60: '#BE7986',
					40: '#D5A5D5',
					20: '#EBD2D5'
				},
				gray: {
					100: 'rgb(117,120,123)',
					80: 'rgb(144,144,147)',
					60: 'rgb(170,170,173)',
					40: 'rgb(198,197,199)',
					20: 'rgb(225,226,226)'
				},
				black: {
					100: 'rgb(0, 0, 0)',
					80: 'rgb(88, 89, 91)',
					60: 'rgb(128, 130, 133)',
					40: 'rgb(167, 169, 172)',
					20: 'rgb(209, 211, 212)'
				}
			}
		}
	}
};
