import React from 'react';
import { graphql } from 'gatsby';
import Img from 'gatsby-image';
import Layout from '../components/layout';
import SEO from '../components/seo';
import { formatDate } from '../components/common';
import styles from '../styles/course.module.css';
import BioList from '../components/bio-list';
import Media from '../components/media';

export const query = graphql`
	query TwoHoursPerWeekTemplate($id: ID!) {
		cms {
			TwoHoursPerWeek(where: { id: $id }) {
				name
				start
				description
				registrationLink
				instructors {
					id
					name
					slug
					image {
						publicUrl
						gatsbyFile {
							childImageSharp {
								fluid(maxWidth: 90, maxHeight: 90) {
									...GatsbyImageSharpFluid
								}
							}
						}
					}
					email
					linkedin
					github
					gitlab
				}

				tags {
					name
				}
				image {
					publicUrl
					gatsbyFile {
						childImageSharp {
							fluid(maxWidth: 700, maxHeight: 700) {
								...GatsbyImageSharpFluid
							}
						}
					}
				}
			}
		}
	}
`;

export default ({ data }) => (
	<Layout className='bg-course' title={data.cms.TwoHoursPerWeek.name}>
		<SEO
			title={data.cms.TwoHoursPerWeek.name}
			keywords={['TwoHoursPerWeeks', ...data.cms.TwoHoursPerWeek.tags.map(({ name }) => name)]}
			description={`IEEE SSCS AlexSC TwoHoursPerWeek${
				data.cms.TwoHoursPerWeek.start ? ` starting from ${formatDate(data.cms.TwoHoursPerWeek.start)}` : ''
			}.`}
			image={data.cms.TwoHoursPerWeek.image.publicUrl}
		/>
		<h1>{data.cms.TwoHoursPerWeek.name}</h1>
		<BioList list={data.cms.TwoHoursPerWeek.instructors} />
		<Img
			fluid={data.cms.TwoHoursPerWeek.image.gatsbyFile.childImageSharp.fluid}
			className={styles.courseImage + ' paper'}
		/>
		<section className='paper p-4 rich-text'>
			<Media content={data.cms.Article.content} />
		</section>
		{data.cms.TwoHoursPerWeek.registrationLink ? (
			<div className='flex-container'>
				<a href={data.cms.TwoHoursPerWeek.registrationLink}>
					<button className={styles.registerationLink}>
						<span>Register</span>
					</button>
				</a>
			</div>
		) : (
			''
		)}
	</Layout>
);
