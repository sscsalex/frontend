import React from 'react';
import parse from 'html-react-parser';
import ReactPlayer from 'react-player';

class Media extends React.Component {
	replacerFunc() {
		let html = this.props.content;
		let replace = domNode => {
			if (domNode.attribs && domNode.attribs.class == 'media') {
				let url = domNode.children[0].attribs.url;
				return <ReactPlayer className='m-4' url={url} controls={true} width='100%' height='100%' />;
			}
		};
		return parse(html, { replace });
	}
	render() {
		return this.replacerFunc();
	}
}

export default Media;
